# Generative Art Playground

Where I store examples of generative art in Python I found across the web.

## Sprite Generator by Eric Davidson (spritething.py)

This generates pixel art sprites with some random color options, and its output resembles multi-colored space invaders.

Code from:  
https://www.freecodecamp.org/news/how-to-create-generative-art-in-less-than-100-lines-of-code-d37f379859f/

To use:  
`python spritething.py [SPRITE_DIMENSIONS] [NUMBER] [IMAGE_SIZE]`

